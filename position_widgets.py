import tkinter as tk
import time
import logging, logg

# Useful third party website
# https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/frame.html
# https://tkdocs.com/tutorial/widgets.html 
# https://www.tutorialspoint.com/python3/python_gui_programming.htm 


# current file logger
cf_logger = logging.getLogger('Tk_widgets_sample.position_widgets')

def grid_sample():
    # get master
    master = tk.Tk()

    # init btn
    btn1 = tk.Button(master)
    btn1['width'] = 10
    btn1['height'] = 2
    btn1['text'] = 'grid test'

    btn2 = tk.Button(master)
    btn2['width'] = 10
    btn2['height'] = 2
    btn2['text'] = 'grid test'

    btn3 = tk.Button(master)
    btn3['width'] = 10
    btn3['height'] = 2
    btn3['text'] = 'grid test'

    btn4 = tk.Button(master)
    btn4['width'] = 10
    btn4['height'] = 2
    btn4['text'] = 'grid test'

    # https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/grid.html

    # column	The column number where you want the widget gridded, counting from zero. The default value is zero.
    # columnspan	Normally a widget occupies only one cell in the grid. However, you can grab multiple cells of a row and merge them into one larger cell by setting the columnspan option to the number of cells. For example, w.grid(row=0, column=2, columnspan=3) would place widget w in a cell that spans columns 2, 3, and 4 of row 0.
    # in_	To register w as a child of some widget w2, use in_=w2. The new parent w2 must be a descendant of the parent widget used when w was created.
    # ipadx	Internal x padding. This dimension is added inside the widget inside its left and right sides.
    # ipady	Internal y padding. This dimension is added inside the widget inside its top and bottom borders.
    # padx	External x padding. This dimension is added to the left and right outside the widget.
    # pady	External y padding. This dimension is added above and below the widget.
    # row	The row number into which you want to insert the widget, counting from 0. The default is the next higher-numbered unoccupied row.
    # rowspan	Normally a widget occupies only one cell in the grid. You can grab multiple adjacent cells of a column, however, by setting the rowspan option to the number of cells to grab. This option can be used in combination with the columnspan option to grab a block of cells. For example, w.grid(row=3, column=2, rowspan=4, columnspan=5) would place widget w in an area formed by merging 20 cells, with row numbers 3–6 and column numbers 2–6.
    # sticky	This option determines how to distribute any extra space within the cell that is not taken up by the widget at its natural size. See below.

    btn1.grid(row=0, column=0, padx=5, pady=5, ipadx=5, ipady=5, sticky=tk.N + tk.S)
    btn2.grid(row=0, column=1, padx=5, pady=5, ipadx=5, ipady=5, sticky=tk.N + tk.S)
    btn3.grid(row=1, column=0, padx=5, pady=5, ipadx=5, ipady=5, sticky=tk.N + tk.S)
    btn4.grid(row=1, column=1, padx=5, pady=5, ipadx=5, ipady=5, sticky=tk.N + tk.S)

    #mainloop
    master.mainloop()

    # Other methods of grid, and resize windows methods of grid

    # https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/grid-methods.html 
    # https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/grid-config.html
    # https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/root-resize.html 


def pack_sample():
    # get master
    master = tk.Tk()

    # init btn
    btn1 = tk.Button(master)
    btn1['width'] = 10
    btn1['height'] = 2
    btn1['text'] = 'grid test'

    btn2 = tk.Button(master)
    btn2['width'] = 10
    btn2['height'] = 2
    btn2['text'] = 'grid test'

    btn3 = tk.Button(master)
    btn3['width'] = 10
    btn3['height'] = 2
    btn3['text'] = 'grid test'

    btn4 = tk.Button(master)
    btn4['width'] = 10
    btn4['height'] = 2
    btn4['text'] = 'grid test'

    # Pack a widget in the parent widget. Use as options:

    # after=widget - pack it after you have packed widget
    # anchor=NSEW (or subset) - position widget according to given direction
    # before=widget - pack it before you will pack widget
    # expand=bool - expand widget if parent size grows
    # fill=NONE or X or Y or BOTH - fill widget if widget grows
    # in=master - use master to contain this widget
    # in_=master - see 'in' option description
    # ipadx=amount - add internal padding in x direction
    # ipady=amount - add internal padding in y direction
    # padx=amount - add padding in x direction
    # pady=amount - add padding in y direction
    # side=TOP or BOTTOM or LEFT or RIGHT -  where to add this widget.

    btn1.pack(expand=True, fill=tk.BOTH, padx=5, pady=5, ipadx=5, ipady=5, side=tk.TOP)
    btn2.pack(expand=True, fill=tk.BOTH, padx=5, pady=5, ipadx=5, ipady=5, side=tk.BOTTOM)
    btn3.pack(expand=True, fill=tk.BOTH, padx=5, pady=5, ipadx=5, ipady=5, side=tk.LEFT)
    btn4.pack(expand=True, fill=tk.BOTH, padx=5, pady=5, ipadx=5, ipady=5, side=tk.RIGHT)

    #mainloop
    master.mainloop()


def place_sample():
    # get master
    master = tk.Tk()

    # init btn
    btn1 = tk.Button(master)
    btn1['width'] = 10
    btn1['height'] = 2
    btn1['text'] = 'grid test1'

    btn2 = tk.Button(master)
    btn2['width'] = 10
    btn2['height'] = 2
    btn2['text'] = 'grid test2'

    btn3 = tk.Button(master)
    btn3['width'] = 10
    btn3['height'] = 2
    btn3['text'] = 'grid test3'

    btn4 = tk.Button(master)
    btn4['width'] = 10
    btn4['height'] = 2
    btn4['text'] = 'grid test4'

    # Place a widget in the parent widget. Use as options:

    # in=master - master relative to which the widget is placed
    # in_=master - see 'in' option description
    # x=amount - locate anchor of this widget at position x of master
    # y=amount - locate anchor of this widget at position y of master
    # relx=amount - locate anchor of this widget between 0.0 and 1.0 relative to width of master (1.0 is right edge)
    # rely=amount - locate anchor of this widget between 0.0 and 1.0 relative to height of master (1.0 is bottom edge)
    # anchor=NSEW (or subset) - position anchor according to given direction
    # width=amount - width of this widget in pixel
    # height=amount - height of this widget in pixel
    # relwidth=amount - width of this widget between 0.0 and 1.0 relative to width of master (1.0 is the same width as the master)
    # relheight=amount - height of this widget between 0.0 and 1.0 relative to height of master (1.0 is the same height as the master)
    # bordermode="inside" or "outside" - whether to take border width of master widget into account

    btn1.place(x=200, y=200, anchor=tk.CENTER, width=100,  bordermode='inside')
    btn2.place(relx=0.4, rely=0.4, anchor=tk.N+tk.E, width=100, bordermode='outside')
    btn3.place(relx=0.6, rely=0.6, anchor=tk.N+tk.E, width=100, bordermode='inside')
    btn4.place(relx=0.7, rely=0.7, anchor=tk.N+tk.E, width=100, bordermode='outside')

    #mainloop
    master.mainloop()


if __name__ == '__main__':
    place_sample()