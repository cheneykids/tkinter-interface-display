from win import Win
from advanced_widgets import *

# some functions are not testing myself, if any bugs please fix first then run again
# it should be a minor problem

def test_Toplevel():
    toplevel = ToplevelCustom()
    toplevel.resource_set_test()
    toplevel.methods_entry_test()

def test_PanedWindow():
    pw = PanedWindowCustom()
    pw.resource_set_test()
    pw.methods_entry_test()

def test_Listbox():
    lb = ListboxCustom()
    lb.resource_set_test()
    lb.methods_entry_test()

def test_Spinbox():
    sb = SpinboxCustom()
    sb.resource_set_test()
    sb.methods_entry_test()

def test_Textbox():
    tx = TextboxCustom()
    tx.resource_set_test()
    tx.methods_entry_test()

def test_ScrolledTextbox():
    tx = ScrolledTextCustom()
    tx.resource_set_test()
    tx.methods_entry_test()

def test_BitmapImagetbox():
    image = BitmapImageCustom()

def test_PhotoImagetbox():
    image = PhotoImageCustom()

def test_Canvasbox():
    canvas = CanvasCustom()
    canvas.resource_set_test()
    canvas.methods_entry_test()

if __name__ == '__main__':
    test_Toplevel()