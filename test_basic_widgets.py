from win import Win
from basic_widgets import *

# some functions are not testing myself, if any bugs please fix first then run again
# it should be a minor problem

def test_win():
    win = Win()
    win.check_configure()
    win.run()

def test_Frame():
    frm = FrameCustom()
    frm.resource_set_test()

def test_LabelFrame():
    label_frm = LabelFrameCustom()
    label_frm.resource_set_test()

def test_Label():
    label = LabelCustom()
    label.resource_set_test()

def test_Entry():
    entry = EntryCustom()
    entry.resource_set_test()
    entry.methods_entry_test()

def test_Scrollbar():
    scrollbar = ScrollbarCustom()
    scrollbar.resource_set_test()
    scrollbar.methods_entry_test()

def test_Button():
    btn = ButtonCustom()
    btn.resource_set_test()
    btn.methods_entry_test()

def test_Checkbutton():
    checkbutton = CheckbuttonCustom()
    checkbutton.resource_set_test()
    checkbutton.methods_entry_test()

def test_Radiobutton():
    radiobutton = RadiobuttonCustom()
    radiobutton.resource_set_test()
    radiobutton.methods_entry_test()

def test_Scale():
    scale = ScaleCustom()
    scale.resource_set_test()
    scale.methods_entry_test()

if __name__ == '__main__':
    test_win()

