import tkinter as tk
import time
import logging, logg
import tkinter.ttk as ttk

# Useful third party website
# https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/frame.html
# https://tkdocs.com/tutorial/widgets.html 
# https://www.tutorialspoint.com/python3/python_gui_programming.htm 


# current file logger
cf_logger = logging.getLogger('Tk_widgets_sample.universal_functions')

def universal_function():
    # https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/universal.html
    # details refer to above links

    # common use the bind function

    # get master
    master = tk.Tk()

    style = ttk.Style()
    # gloabal default style
    style.configure('.', font='Arial 19', foreground='yellow', background='red')

    # custom style
    style.configure('mystyle.TButton', font='Arial 15', foreground='blue', padding=10)

    # buttons
    btn1 = ttk.Button(master, text='Im using default gloable style')
    btn1.pack()

    btn2 = ttk.Button(master, text='Im using mystyle', style='mystyle.TButton')
    btn2.pack()

    #bind
    master.bind("<Button-3>", right_click) # right click with mouse
    master.bind("<KeyPress-F2>", press_F2)# press F2

    master.mainloop()

def right_click(event):
    cf_logger.info('right click with bind function.{}'.format(event))

def press_F2(event):
    cf_logger.info('F2 press with bind function. {}'.format(event))