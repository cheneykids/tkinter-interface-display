import tkinter as tk
import time
import logging, logg

# Useful third party website
# https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/index.html
# https://tkdocs.com/tutorial/widgets.html 
# https://www.tutorialspoint.com/python3/python_gui_programming.htm 

# note format:
    # resource name: 
    # note:
    # resource value:

# current file logger
cf_logger = logging.getLogger('Tk_widgets_sample.advanced_components')

class MenuCustom(object):
    def __init__(self, master=None):
        # get master
        self.master = master

        if self.master is None:
            self.master = tk.Tk()

        # init toplevel
        self.toplevel = tk.Toplevel(self.master)
        self.toplevel['width'] = 200
        self.toplevel['height'] = 100

        # init menu
        self.menubar = tk.Menu(self.toplevel)
        self.toplevel['menu'] = self.menubar

        self.childmenu = tk.Menu(self.menubar, tearoff=False)
        self.childmenu.add_command(label='option 1')

        self.toplevel.pack()

    def resource_set(self, key, value):
        """
        Set the widget
        :param key: resource name
        :param value: value
        :return: None
        """
        # Setting method:
        # config -> self.menu.config(bg='red'), self.menu.config(background='red'),
        # configure -> self.menu.configure(bg='red'), self.menu.configure(background='red'),
        # self.menu['bg'] = 'red', self.menu['background'] = 'red'

        # Valid resource names: 
        # activebackground, activeborderwidth,
        # activeforeground, background, bd, bg, borderwidth, cursor,
        # disabledforeground, fg, font, foreground, postcommand, relief,
        # selectcolor, takefocus, tearoff, tearoffcommand, title, type.

        self.menu[key] = value

    def resource_set_test(self):
        """
        Set the widget resource name with different value
        Try postcommand, tearoff, tearoffcommand, title

        Other resources refer to basic_widgets.py
        """
        # resource name: postcommand
        # note: You can set this option to a procedure, and that procedure will be called every time someone brings up this menu.
        # resource value: method
        self.resource_set('postcommand', menu_postcommand_callback)
        cf_logger.info('MenuCustom object set postcommand.')

        # resource name: tearoff
        # note: Normally, a menu can be torn off: the first position (position 0) in the list of choices is occupied by the tear-off 
        # element, and the additional choices are added starting at position 1. If you set tearoff=0, the menu will not have a tear-off 
        # feature, and choices will be added starting at position 0.
        # resource value: True or False
        self.resource_set('tearoff', True)
        cf_logger.info('MenuCustom object set tearoff.')

        # resource name: tearoffcommand
        # note: If you would like your program to be notified when the user clicks on the tear-off entry in a menu, set this option 
        # to your procedure. It will be called with two arguments: the window ID of the parent window, and the window ID of the new 
        # tear-off menu's root window.
        # resource value: method
        self.resource_set('tearoffcommand', menu_tearoff_callback)
        cf_logger.info('MenuCustom object set tearoffcommand.')

        # resource name: title
        # note: Normally, the title of a tear-off menu window will be the same as the text of the menubutton or cascade that lead to 
        # this menu. If you want to change the title of that window, set the title option to that string.
        # resource value: string
        self.resource_set('title', 'menu title')
        cf_logger.info('MenuCustom object set title.')

        # run mainloop
        self.run()

    def run(self):
        # run master mainloop
        self.master.mainloop()

    def menu_postcommand_callback(self):
        cf_logger.info('postcommand meun.')
    
    def menu_tearoff_callback(self, id1, id2):
        cf_logger.info('tearoff command of menu, id1 {} id2 {}'.format(id1, id2))

    def methods_menu_test(self):
        """
        Try all common methods of Menu object.
        Not really run below methods, just try to understand it by doc.

        """
        # refer to below linkds about menu methods
        # https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/menu.html 
        # https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/menu-coptions.html

    
class MessageboxCustom(object):
    def __init__(self, master=None):
        # get master
        self.master = master

        if self.master is None:
            self.master = tk.Tk()

        # use messagebox functions
        # https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/tkMessageBox.html 
        import tkinter.messagebox as mb
        cf_logger.info('showinfo', mb.showinfo(title='showinfo', message='showinfo test'))
        cf_logger.info('showwarning', mb.showwarning(title='showwarning', message='showwarning test'))
        cf_logger.info('showerror', mb.showerror(title='showerror', message='showerror test'))
        cf_logger.info('askquestion', mb.askquestion(title='askquestion', message='askquestion test'))
        cf_logger.info('askokcancel', mb.askokcancel(title='askokcancel', message='askokcancel test'))
        cf_logger.info('askyesno', mb.askyesno(title='askyesno', message='askyesno test'))
        cf_logger.info('askyesnocancel', mb.askyesnocancel(title='askyesnocancel', message='askyesnocancel test'))
        cf_logger.info('askretrycancel', mb.askretrycancel(title='askretrycancel', message='askretrycancel test'))


class ColorchooserCustom(object):
    def __init__(self, master=None):
        # get master
        self.master = master

        if self.master is None:
            self.master = tk.Tk()

        # use colorchooser functions with 2 ways
        import tkinter.colorchooser as cc
        cf_logger.info('colorchoose', cc.Chooser().show()) #1st 

        cf_logger.info('colorchoose', cc.askcolor()) # 2nd 


class FiledialogCustom(object):
    def __init__(self, master=None):
        # get master
        self.master = master

        if self.master is None:
            self.master = tk.Tk()

        # ask filedialog
        # https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/tkFileDialog.html 
        import tkinter.filedialog as fd
        cf_logger.info('askopenfilename : {}'.format(fd.askopenfilename()))
        cf_logger.info('askopenfile : {}'.format(fd.askopenfile('r')))
        cf_logger.info('askopenfiles : {}'.format(fd.askopenfiles('r')))
        cf_logger.info('askdirectory : {}'.format(fd.askdirectory()))
        cf_logger.info('askopenfilenames : {}'.format(fd.askopenfilenames()))
        cf_logger.info('asksaveasfilename : {}'.format(fd.asksaveasfilename()))
        cf_logger.info('asksaveasfile : {}'.format(fd.asksaveasfile('w')))

       