import tkinter as tk
import time
import logging, logg
import tkinter.ttk as ttk

# Useful third party website
# https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/frame.html
# https://tkdocs.com/tutorial/widgets.html 
# https://www.tutorialspoint.com/python3/python_gui_programming.htm 


# current file logger
cf_logger = logging.getLogger('Tk_widgets_sample.ttk_widgets')

def ttk_styles():
    # https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/ttk-style-layer.html

    # get master
    master = tk.Tk()

    style = ttk.Style()
    # gloabal default style
    style.configure('.', font='Arial 19', foreground='yellow', background='red')

    # custom style
    style.configure('mystyle.TButton', font='Arial 15', foreground='blue', padding=10)

    # buttons
    btn1 = ttk.Button(master, text='Im using default gloable style')
    btn1.pack()

    btn2 = ttk.Button(master, text='Im using mystyle', style='mystyle.TButton')
    btn2.pack()

    master.mainloop()


if __name__ == '__main__':
    ttk_styles()
