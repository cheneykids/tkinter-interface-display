from win import Win
import tkinter as tk

def test_win():
    win = Win()
    win.check_configure()
    win.run()

def test_btn():
    root = tk.Tk()
    btn = tk.Button(root)
    btn.configure(width=20, height=3, text='ButtonCustom test')
    btn.pack()

    btn['state'] = tk.NORMAL
    btn['default'] = tk.DISABLED
    btn['overrelief'] = tk.GROOVE
    btn.config(repeatdelay=500, repeatinterval=100)
    btn['command'] = _btn_callback
    btn['compound'] = 'left'

    btn['bg'] = 'red'
    btn['activebackground'] = 'yellow'

    root.mainloop()

def _btn_callback():
    print('btn callback')

def test_checkbtn():
    root = tk.Tk()
    btn = tk.Checkbutton(root)
    btn.configure(width=20, height=1, text='ButtonCustom test')
    btn.pack()

    btn['indicatoron'] = 1

    btn['offvalue'] = 0
    btn['onvalue'] = 1

    ivar = tk.IntVar()
    btn['variable'] = ivar
    #ivar.set(6)
    #print('value {}'.format(ivar.get()))
    btn['command'] = checkbtn_callback
    ivar.set(1)

    btn.deselect()

    root.mainloop()

def checkbtn_callback():
    print('checkbtn callback')


def test_radiokbtn():
    root = tk.Tk()

    ivar_radio = tk.IntVar()

    btn1 = tk.Radiobutton(root)
    btn1.configure(width=20, height=1, text='ButtonCustom test1', value=1, variable=ivar_radio)
    btn1.pack()

    btn2 = tk.Radiobutton(root)
    btn2.configure(width=20, height=1, text='ButtonCustom test3', value=2, variable=ivar_radio)
    btn2.pack()

    btn3 = tk.Radiobutton(root)
    btn3.configure(width=20, height=1, text='ButtonCustom test3', value=3, variable=ivar_radio)
    btn3.pack()

    btn1['command'] = radiobutton_callback
    btn2['command'] = radiobutton_callback
    btn3['command'] = radiobutton_callback

    print(ivar_radio.get())
    ivar_radio.set(6)
    print(ivar_radio.get())

    root.mainloop()

def radiobutton_callback():
    print('radiobutton callback')

def test_scale():
    root = tk.Tk()
    scale = tk.Scale(root)
    scale['bigincrement'] = 5
    scale['digits'] = 6

    scale['label'] = 'scale title'
    scale['from'] = 20
    scale['to'] = 50
    scale['showvalue'] = True
    scale['resolution'] = 6
    scale['sliderrelief'] = 'groove'
    scale['tickinterval'] = 6

    ivar = tk.IntVar()
    scale['variable'] = ivar
    ivar.set(35)

    scale['command'] = scale_callback

    coords = scale.coords(30)
    print('coords {}'.format(coords))

    id = scale.identify(0, 0)
    print('id: {}'.format(id))

    scale.pack()

    root.mainloop()

def scale_callback(self):
    print(self)
    print('scale_callback')

def test_toplevel():
    root = tk.Tk()
    btn = tk.Button(root)
    btn.configure(width=20, height=3, text='ButtonCustom test')
    btn.pack()

    btn['command'] = _btn_tl_callback
    btn['bg'] = 'red'
    btn['activebackground'] = 'yellow'

    root.mainloop()


def _btn_tl_callback():
    toplevel = tk.Toplevel()
    toplevel.configure(width=200, height=100)
    #label = tk.Label(tl, text='toplevel window')
    #label.pack()
    print('btn callback')

    # menubar
    menubar = tk.Menu(toplevel)
    toplevel['menu'] = menubar

    # top level menu click command
    menubar.add_command(label='Topmenu_click', command=menu_callback)

    # top level cascade
    cascade_menu = tk.Menu(menubar, tearoff=False)
    cascade_menu.add_command(label='cascade option 1st', command=menu_callback)
    cascade_menu.add_command(label='cascade option 2ndt', command=menu_callback)

    menubar.add_cascade(label='cascade_name', menu=cascade_menu)

    # drop list menu cascade
    droplist_cascade_menu = tk.Menu(cascade_menu, tearoff=False)
    droplist_cascade_menu.add_command(label='droplist cascade option 1st', command=menu_callback)
    droplist_cascade_menu.add_command(label='droplist cascade option 2nd', command=menu_callback)

    cascade_menu.add_cascade(label='droplist_cascade_name', menu=droplist_cascade_menu)

    toplevel.aspect(1, 1, 2, 1)
    toplevel.iconify()
    toplevel.deiconify()
    toplevel.wm_deiconify()
    toplevel.geometry('300x100')
    #toplevel.lift()
    print(toplevel.minsize())
    print(toplevel.maxsize())
    toplevel.minsize(5,5)
    toplevel.maxsize(500,400)
    print(toplevel.overrideredirect())
    toplevel.overrideredirect(False)

    toplevel.state('zoomed')
    toplevel.wm_state()
    toplevel.title('Toplevel title')
    toplevel.resizable(True, True)
    #toplevel.wm_resizable(True, False)

    #toplevel.withdraw()
    # toplevel.wm_withdraw()

def menu_callback():
    print('click meun.')


def test_panedwindow():
    # bot paned win & label
    root = tk.Tk()
    pw_layer_bot = tk.PanedWindow(root)
    pw_layer_bot.pack(fill='both', expand= 1)

    label_layer_bot = tk.Label(pw_layer_bot, text='left')
    pw_layer_bot.add(label_layer_bot)

    # top paned win & label
    pw_layer_top = tk.PanedWindow(pw_layer_bot, orient=tk.VERTICAL)
    pw_layer_bot.add(pw_layer_top)

    label_layer_top_1 = tk.Label(pw_layer_top, text='right top')
    pw_layer_top.add(label_layer_top_1)

    label_layer_top_2 = tk.Label(pw_layer_top, text='right bottom')
    pw_layer_top.add(label_layer_top_2)

    pw_layer_bot['showhandle'] =True
    pw_layer_top['showhandle'] = True

    pw_layer_bot['handlepad'] = 10
    pw_layer_bot['handlesize'] = 10

    pw_layer_bot['opaqueresize'] = False
    pw_layer_bot['sashcursor'] = 'cross'
    pw_layer_bot['sashpad'] = 40
    pw_layer_bot['sashwidth'] = 60

    pw_layer_bot.paneconfigure(label_layer_bot, height=50)
    print(pw_layer_bot.panecget(label_layer_bot, 'height'))

    widgets_list = pw_layer_top.panes()
    print('widgets_list: {}'.format(widgets_list))

    print(pw_layer_bot.sash_coord(0))

    pw_layer_bot.sash_place(0, 20, 10)

    print('btn callback')

    root.mainloop()

def test_panedwindow_single():
    root = tk.Tk()
    pw_layer_bot = tk.PanedWindow(root)
    #pw_layer_bot.pack(fill='both', expand=1)
    pw_layer_bot.pack()

    pw_layer_bot.config(width=200, height=100, bg='red')

    root.mainloop()

def test_listbox():
    root = tk.Tk()
    # lb
    lb = tk.Listbox(root)
    lb.configure(width=10, height=10)

    # sb
    xs = tk.Scrollbar(root, orient=tk.HORIZONTAL)
    ys = tk.Scrollbar(root, orient=tk.VERTICAL)
    xs.pack(side=tk.TOP, fill='both')
    ys.pack(side=tk.RIGHT, fill='both')

    #config sb & lb
    xs.config(command=lb.xview)
    ys.config(command=lb.yview)
    lb.config(xscrollcommand=xs.set, yscrollcommand=ys.set)

    #config lb
    lb.insert(1, "Python")
    lb.insert(2, "Perl")
    lb.insert(3, "C")
    lb.insert(4, "PHP")
    lb.insert(5, "JSP")
    lb.insert(6, "Ruby")
    lb.pack()

    #listCon = tk.StringVar()
    #lb['listvariable'] = listCon
    #listCon.set('ant bee cicada ')
    #print(listCon.get())

    lb['activestyle'] = 'dotbox'
    lb['setgrid'] = False

    lb.see(2)
    #methods
    lb.activate(2)
    #print(listCon.get())
    lb.index(2)
    bb = lb.bbox(2)
    print('bounding box {}'.format(bb))

    lb.see(1)
    lb.itemconfig(1, background='red')
    print(lb.itemcget(1, 'background'))

    print(lb.size())

    root.mainloop()

def test_spinbox():
    root = tk.Tk()
    # sb
    sb = tk.Spinbox(root)
    sb.configure(width=10, from_=0, to=20)

    sb.config(insertbackground='red', selectbackground='yellow')
    sb.config(buttoncursor='cross')
    sb['increment'] = 5
    sb['command'] = spinbox_callback

    #sb['values'] = ('red', 'blue', 'green')

    #methods
    sb.delete(0,2)

    sb.pack()

    root.mainloop()

def spinbox_callback():
    print('spinbox callback.')

def test_text():
    root = tk.Tk()
    # sb
    tx = tk.Text(root)
    tx.configure(width=50, height=20)

    tx['undo'] = True
    tx['maxundo'] = 2
    tx['autoseparators'] = True

    tx['spacing1'] = 3
    tx['spacing2'] = 6
    tx['spacing3'] = 9

    tx['wrap'] = tk.NONE
    tx.insert(1.0, 'hello world.')
    #methods
    print(tx.compare(1.0, '<', tk.END))
    tx.image_create(1.1, align=tk.TOP, image='', name='ima')
    print(tx.search(' ', 1.0, nocase=True))

    tx['takefocus'] = True
    tx.mark_set('mark#1', 1.2)
    print(tx.mark_names())
    print(tx.mark_gravity('insert'))
    print(tx.mark_gravity('mark#1'))

    print(tx.mark_previous(1.3))
    tx.tag_add('tag#1', 1.0, 1.5)

    tx.tag_bind('tag#1', '<Button-1>', click_callback)
    tx.tag_config('tag#1', background='red', foreground='blue')

    print(tx.tag_prevrange('tag#1', 1.1, 1.2))
    print(tx.tag_ranges('tag#1'))

    tx.tag_unbind('tag#1', '<Button-1>')

    tx_child = tk.Text(tx)
    tx_child.config(width=10, heigh=5)
    tx_child.insert(1.0, 'hello child text')
    tx_child.pack()

    tx.window_create(1.1, window=tx_child)

    print(tx.window_names())

    tx.pack()

    root.mainloop()

def click_callback(event):
    print('tag callback.')

def test_bitmapImage():
    root = tk.Tk()
    # sb
    bm = tk.BitmapImage('error')
    lb = tk.Label(root, bitmap=bm)
    lb.pack()

    root.mainloop()

def test_canvas():
    root = tk.Tk()
    # sb
    canvas = tk.Canvas(root)
    canvas.configure(width=200, height=200)

    canvas.pack()
    canvas['offset'] = "5"

    root.mainloop()


def test_menu():
    root = tk.Tk()
    btn = tk.Button(root)
    btn.configure(width=20, height=3, text='ButtonCustom test')
    btn.pack()

    btn['command'] = _btn_menu_callback
    btn['bg'] = 'red'
    btn['activebackground'] = 'yellow'

    root.mainloop()

def _btn_menu_callback():
    toplevel = tk.Toplevel()
    toplevel.configure(width=200, height=100)
    print('btn callback')

    # menubar
    menubar = tk.Menu(toplevel)
    toplevel['menu'] = menubar

    childmenu = tk.Menu(menubar, tearoff=False)
    childmenu.add_command(label='option 1')
    childmenu.add_separator()
    childmenu.add_checkbutton(background='red', label='checked')
    childmenu.add_separator()
    childmenu.add_radiobutton(background='yellow', label='radio')
    childmenu.add_command(label='option 2')

    childmenu['postcommand'] = _postcommand_menu_callback
    childmenu['tearoff'] = True
    childmenu['tearoffcommand'] = _tearoff_menu_callback
    childmenu['title'] = 'childmenu tilte'

    menubar.add_cascade(label='childmenu', menu=childmenu)


def _postcommand_menu_callback():
    print('postcommand of menu.')

def _tearoff_menu_callback(id1,id2):
    print('tearoff command of menu, id1 {} id2 {}'.format(id1, id2))

def test_messagebox():
    import tkinter.messagebox as mb
    mb.showinfo(title='showinfo', message='showinfo test')
    mb.showwarning(title='showwarning', message='showwarning test')
    mb.showerror(title='showerror', message='showerror test')
    mb.askquestion(title='askquestion', message='askquestion test')
    mb.askokcancel(title='askokcancel', message='askokcancel test')
    mb.askyesno(title='askyesno', message='askyesno test')
    mb.askyesnocancel(title='askyesnocancel', message='askyesnocancel test')
    mb.askretrycancel(title='askretrycancel', message='askretrycancel test')

def test_colorchoose():
    import tkinter.colorchooser as cc
    cc.Chooser().show()

    cc.askcolor()

def test_filedialog():
    import tkinter.filedialog as fd
    print('askopenfilename : {}'.format(fd.askopenfilename()))
    print('askopenfile : {}'.format(fd.askopenfile('r')))
    print('askopenfiles : {}'.format(fd.askopenfiles('r')))
    print('askdirectory : {}'.format(fd.askdirectory()))
    print('askopenfilenames : {}'.format(fd.askopenfilenames()))
    print('asksaveasfilename : {}'.format(fd.asksaveasfilename()))
    print('asksaveasfile : {}'.format(fd.asksaveasfile('w')))


def grid_sample():
    # get master
    master = tk.Tk()

    # init frame
    btn1 = tk.Button(master)
    btn1['width'] = 10
    btn1['height'] = 2
    btn1['text'] = 'grid test'

    btn2 = tk.Button(master)
    btn2['width'] = 10
    btn2['height'] = 2
    btn2['text'] = 'grid test'

    btn3 = tk.Button(master)
    btn3['width'] = 10
    btn3['height'] = 2
    btn3['text'] = 'grid test'

    btn4 = tk.Button(master)
    btn4['width'] = 10
    btn4['height'] = 2
    btn4['text'] = 'grid test'

    # https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/grid.html

    # column	The column number where you want the widget gridded, counting from zero. The default value is zero.
    # columnspan	Normally a widget occupies only one cell in the grid. However, you can grab multiple cells of a row and merge them into one larger cell by setting the columnspan option to the number of cells. For example, w.grid(row=0, column=2, columnspan=3) would place widget w in a cell that spans columns 2, 3, and 4 of row 0.
    # in_	To register w as a child of some widget w2, use in_=w2. The new parent w2 must be a descendant of the parent widget used when w was created.
    # ipadx	Internal x padding. This dimension is added inside the widget inside its left and right sides.
    # ipady	Internal y padding. This dimension is added inside the widget inside its top and bottom borders.
    # padx	External x padding. This dimension is added to the left and right outside the widget.
    # pady	External y padding. This dimension is added above and below the widget.
    # row	The row number into which you want to insert the widget, counting from 0. The default is the next higher-numbered unoccupied row.
    # rowspan	Normally a widget occupies only one cell in the grid. You can grab multiple adjacent cells of a column, however, by setting the rowspan option to the number of cells to grab. This option can be used in combination with the columnspan option to grab a block of cells. For example, w.grid(row=3, column=2, rowspan=4, columnspan=5) would place widget w in an area formed by merging 20 cells, with row numbers 3–6 and column numbers 2–6.
    # sticky	This option determines how to distribute any extra space within the cell that is not taken up by the widget at its natural size. See below.

    btn1.grid(row=0, column=0, padx=5, pady=5, ipadx=5, ipady=5, sticky=tk.N + tk.S)
    btn2.grid(row=0, column=1, padx=5, pady=5, ipadx=5, ipady=5, sticky=tk.N + tk.S)
    btn3.grid(row=1, column=0, padx=5, pady=5, ipadx=5, ipady=5, sticky=tk.N + tk.S)
    btn4.grid(row=1, column=1, padx=5, pady=5, ipadx=5, ipady=5, sticky=tk.N + tk.S)
    master.mainloop()

def pack_sample():
    # get master
    master = tk.Tk()

    # init btn
    btn1 = tk.Button(master)
    btn1['width'] = 10
    btn1['height'] = 2
    btn1['text'] = 'grid test'

    btn2 = tk.Button(master)
    btn2['width'] = 10
    btn2['height'] = 2
    btn2['text'] = 'grid test'

    btn3 = tk.Button(master)
    btn3['width'] = 10
    btn3['height'] = 2
    btn3['text'] = 'grid test'

    btn4 = tk.Button(master)
    btn4['width'] = 10
    btn4['height'] = 2
    btn4['text'] = 'grid test'

    # Pack a widget in the parent widget. Use as options:

    # after=widget - pack it after you have packed widget
    # anchor=NSEW (or subset) - position widget according to given direction
    # before=widget - pack it before you will pack widget
    # expand=bool - expand widget if parent size grows
    # fill=NONE or X or Y or BOTH - fill widget if widget grows
    # in=master - use master to contain this widget
    # in_=master - see 'in' option description
    # ipadx=amount - add internal padding in x direction
    # ipady=amount - add internal padding in y direction
    # padx=amount - add padding in x direction
    # pady=amount - add padding in y direction
    # side=TOP or BOTTOM or LEFT or RIGHT -  where to add this widget.

    btn1.pack(expand=True, fill=tk.BOTH, padx=5, pady=5, ipadx=5, ipady=5, side=tk.TOP)
    btn2.pack(expand=True, fill=tk.BOTH, padx=5, pady=5, ipadx=5, ipady=5, side=tk.BOTTOM)
    btn3.pack(expand=True, fill=tk.BOTH, padx=5, pady=5, ipadx=5, ipady=5, side=tk.LEFT)
    btn4.pack(expand=True, fill=tk.BOTH, padx=5, pady=5, ipadx=5, ipady=5, side=tk.RIGHT)

    #mainloop
    master.mainloop()

def place_sample():
    # get master
    master = tk.Tk()

    # init btn
    btn1 = tk.Button(master)
    btn1['width'] = 10
    btn1['height'] = 2
    btn1['text'] = 'grid test1'

    btn2 = tk.Button(master)
    btn2['width'] = 10
    btn2['height'] = 2
    btn2['text'] = 'grid test2'

    btn3 = tk.Button(master)
    btn3['width'] = 10
    btn3['height'] = 2
    btn3['text'] = 'grid test3'

    btn4 = tk.Button(master)
    btn4['width'] = 10
    btn4['height'] = 2
    btn4['text'] = 'grid test4'

    # Place a widget in the parent widget. Use as options:

    # in=master - master relative to which the widget is placed
    # in_=master - see 'in' option description
    # x=amount - locate anchor of this widget at position x of master
    # y=amount - locate anchor of this widget at position y of master
    # relx=amount - locate anchor of this widget between 0.0 and 1.0 relative to width of master (1.0 is right edge)
    # rely=amount - locate anchor of this widget between 0.0 and 1.0 relative to height of master (1.0 is bottom edge)
    # anchor=NSEW (or subset) - position anchor according to given direction
    # width=amount - width of this widget in pixel
    # height=amount - height of this widget in pixel
    # relwidth=amount - width of this widget between 0.0 and 1.0 relative to width of master (1.0 is the same width as the master)
    # relheight=amount - height of this widget between 0.0 and 1.0 relative to height of master (1.0 is the same height as the master)
    # bordermode="inside" or "outside" - whether to take border width of master widget into account

    btn1.place(x=200, y=200, anchor=tk.CENTER, width=100,  bordermode='inside')
    btn2.place(relx=0.4, rely=0.4, anchor=tk.N+tk.E, width=100, bordermode='outside')
    btn3.place(relx=0.6, rely=0.6, anchor=tk.N+tk.E, width=100, bordermode='inside')
    btn4.place(relx=0.7, rely=0.7, anchor=tk.N+tk.E, width=100, bordermode='outside')

    #mainloop
    master.mainloop()


import tkinter.ttk as ttk
def ttk_styles():
    # https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/ttk-style-layer.html

    # get master
    master = tk.Tk()

    style = ttk.Style()
    # gloabal default style
    style.configure('.', font='Arial 19', foreground='yellow', background='red')

    # custom style
    style.configure('mystyle.TButton', font='Arial 15', foreground='blue', padding=10)

    # buttons
    btn1 = ttk.Button(master, text='Im using default gloable style')
    btn1.pack()

    btn2 = ttk.Button(master, text='Im using mystyle', style='mystyle.TButton')
    btn2.pack()

    master.mainloop()

def universal_function():
    # https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/universal.html
    # details refer to above links

    # common use the bind function

    # get master
    master = tk.Tk()

    style = ttk.Style()
    # gloabal default style
    style.configure('.', font='Arial 19', foreground='yellow', background='red')

    # custom style
    style.configure('mystyle.TButton', font='Arial 15', foreground='blue', padding=10)

    # buttons
    btn1 = ttk.Button(master, text='Im using default gloable style')
    btn1.pack()

    btn2 = ttk.Button(master, text='Im using mystyle', style='mystyle.TButton')
    btn2.pack()

    #bind
    master.bind("<Button-3>", right_click) # right click with mouse
    master.bind("<KeyPress-F2>", press_F2)# press F2

    master.mainloop()

def right_click(event):
    print('right click with bind function.{}'.format(event))

def press_F2(event):
    print('F2 press with bind function. {}'.format(event))

if __name__ == '__main__':
    #from tkinter.scrolledtext import ScrolledText
    universal_function()

