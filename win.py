import tkinter as tk
import logging
import logg

logger_win = logging.getLogger('Tk_widgets_sample.win')

class Win(object):
    def __init__(self):
        win_name = 'Tk Widgets Sample'
        win_size = '370x200'
        self.win = tk.Tk()

        self.win.title(win_name)
        self.win.geometry(win_size)

    @property
    def get_win(self):
        return self.win

    def check_configure(self):
        """
        Get configure keys and arguments
        :return: keys and args
        """
        # save data
        configure_resorce = {}

        # seperation
        keys = self.win.keys()
        for key in keys:
            args = self.win.cget(key)
            configure_resorce[key] = args
            logger_win.info('key: {}, args: {}'.format(key, args))

        return configure_resorce

    def run(self):
        """
        Run mainloop
        :return: None
        """
        self.win.mainloop()

