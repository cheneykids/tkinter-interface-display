import logging, os, time

# logger
logger = logging.getLogger('Tk_widgets_sample')
logger.setLevel(logging.DEBUG)

# fmt
fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
formatter = logging.Formatter(fmt)

# stream handler
sh = logging.StreamHandler()
sh.setLevel(logging.INFO)
sh.setFormatter(formatter)

# file handler
path = os.getcwd() + '//' + 'log'
file = "Tk_widgets_sample_{0}_log.txt".format(time.strftime("%Y%m%d%H%M%S", time.localtime()))
filename = path + '//'+ file

fh = logging.FileHandler(filename)
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)

# add handlers to logger
logger.addHandler(sh)
logger.addHandler(fh)

# begin to log
logger.info('Logging function enable.')
