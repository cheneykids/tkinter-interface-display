# tkinter界面展示

#### 介绍
使用tkinter库，尝试演示所有控件的使用。

#### 使用说明

####
主要是通过了解tkinter库内容，把相关常用操作示例出来，自己学习的同时，方便有缘人仿照使用。

####
界面类库，主要是显示（包括样式显示和位置显示）和用户交互，示例主要在这两方面做计划演示。

####
能力有限，仅做认知以内的示例

####
尽量各控件以类方式分开，使用时进行组合

一、主窗口 - win.py                                                                                                                                                                                        
Tk

二、基础控件 - basic_widgets.py, basic_widgets_ttk.py                                                                                                                                                
Frame, LabelFrame, Label, Entry, Button, Checkbutton, Scale, Scrollbar                                                                                                                                
StringVar, IntVar, DoubleVar, BooleanVar

三、高级控件 - advanced_widgets.py, advanced_widgets_ttk.py                                                                                                                                                
Toplevel, PanedWindow, Listbox, Spinbox, Text, scrolledtext, PhotoImage, BitmapImage, Canvas

四、高级组件 - advanced_parts.py, advanced_parts_ttk.py                                                                                                            
Menu, OptionMenu, colorchooser, commondialog, dialog, filedialog, simpledialog, messagebox

五、位置 - place.py                                                                                                                                                                    
Pack, Place, Grid

注意：
主要以演示各控件可用属性，和各属性能使用的值，各控件方法为主。
部分脚本直接线上编写，未实际测试，如遇运行出错，可以自行纠正，应该都是小问题。